package dev.techreview.blog.model.dto;

import dev.techreview.blog.model.Author;
import dev.techreview.blog.model.Post;

public record PostDetails(Post post, Author author) {
}
