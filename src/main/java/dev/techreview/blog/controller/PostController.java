package dev.techreview.blog.controller;

import dev.techreview.blog.model.Post;
import dev.techreview.blog.model.dto.PostDetails;
import dev.techreview.blog.repository.AuthorRepository;
import dev.techreview.blog.repository.PostRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(
        name= "Posts",
        description = "Posts dedicated controller"
)
@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final PostRepository posts;
    private final AuthorRepository authors;

    public PostController(PostRepository posts, AuthorRepository authors) {
        this.posts = posts;
        this.authors = authors;
    }

    @Operation(summary = "Get all posts from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the posts",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post[].class)) }),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Error",
                    content = @Content) })
    @GetMapping
    public Iterable<Post> findAll() {
        return posts.findAll();
    }

    @Operation(summary = "Get a post from its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the post",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Post not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public Post findById(@PathVariable Integer id) {
        return posts.findById(id).orElse(null);
    }

    @Operation(summary = "Get details of a post from its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the post details",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDetails.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Post not found",
                    content = @Content) })
    @GetMapping("/{id}/details")
    public PostDetails getPostDetails(@PathVariable Integer id) {
        Post post = posts.findById(id).orElse(null);
        return new PostDetails(post,authors.findById(post.getAuthor().getId()).get());
    }
}
