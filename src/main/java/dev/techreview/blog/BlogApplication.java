package dev.techreview.blog;

import dev.techreview.blog.model.Author;
import dev.techreview.blog.model.Comment;
import dev.techreview.blog.model.Post;
import dev.techreview.blog.repository.AuthorRepository;
import dev.techreview.blog.repository.PostRepository;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jdbc.core.mapping.AggregateReference;

@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title = "Blog API",
				version = "1.0.0",
				description = "Exemple API for a blog",
				contact = @Contact(
						name = "Enzo",
						email = "enzo.comperat@sii.fr"
				),
				license = @License(
						name = "licence",
						url = "runcodenow"
				)
		)
)
public class BlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(PostRepository posts, AuthorRepository authors) {
		return args -> {
			AggregateReference<Author, Integer> enzo = AggregateReference.to(authors.save(new Author(null, "Enzo","Compérat","enzo.comperat@email.fr","enzo")).id());
			Post post = posts.save(new Post("Hello, World!","Welcome to my blog!", enzo));
			post.addComment(new Comment("Enzo","Super ce commentaire."));
			posts.save(post);
		};
	}
}
